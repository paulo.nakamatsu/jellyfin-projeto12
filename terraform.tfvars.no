# Renomeie este arquivo, ou crie outro, com nome terraform.tfvars
# Depois adicione os valores desejados para as variáveis abaixo.
# Isto evitará que sejam solicitados valores para estas variávies 
# quando forem executados os comandos do terraform.


# Nome do profile criado com AWS CLI com as
# credenciais do IAM.
profile = "default"

# Especifique o nome da tag padrão utilizada nos nomes dos serviços
tag-base = "projeto12"

# Especifique o nome do bucket
nome-bucket = "ct-projeto12-ecs-jf"


# Configurações mínima e máxima para o autoscaling
min-tasks = 1
max-tasks = 8

# Imagem do ECR para baixar a aplicação
# Se usar do Docker Hub terá que habilitar o NAT
image-ecr-uri = "901392891750.dkr.ecr.us-east-1.amazonaws.com/proj12jellyfin"

# Define se serão criados NAT Gateways nas 
# subnets 
use-nat-gateway = false

# Define quantidade de CPU para cada task
ecs-vcpu = (2 * 1024) # 2 vCPU

# Define quantidade de memória para cada task
ecs-memory = (4 * 1024) # 4 GB de RAM