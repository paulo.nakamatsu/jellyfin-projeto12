output "sg-web" {
  value = aws_security_group.sg_projeto_web
}

output "sg-elb" {
  value = aws_security_group.sg_projeto_elb
}

output "sg-efs" {
  value = aws_security_group.sg_projeto_efs
}

output "sg-ecr" {
  value = aws_security_group.sg_projeto_ecr
}