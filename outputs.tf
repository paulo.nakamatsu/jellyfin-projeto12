# output "projeto-efs_id" {
#   value = aws_efs_file_system.projeto-efs.id
# }

output "jellyfin-lb-dns" {
  // Existing output

  // Add the following output to display the CloudWatch Logs group and stream names
  value = {
    lb_dns     = aws_lb.jellyfin.dns_name
    log_group  = aws_cloudwatch_log_group.jellyfin.name
    log_stream = aws_cloudwatch_log_stream.jellyfin.name
  }
}

# resource "local_file" "locust_env" {
#   content  = <<-EOT
# LOADBALANCER_DNS=http://${aws_lb.jellyfin.dns_name}
# API_KEY=<Obter no jellyfin>
#     EOT
#   filename = "./locust-load-test/.env"
# }


# resource "local_file" "k6_env" {
#   content  = <<-EOT
# export const LOADBALANCER_DNS = "http://${aws_lb.jellyfin.dns_name}";
# export const API_KEY = "<Obter no jellyfin>";
#     EOT
#   filename = "./k6-load-test/env.js"
# }

# output "nome-bucket" {
#   value = aws_s3_bucket.projeto-static.bucket
# }

