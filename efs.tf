# # Criando EFS1
# resource "aws_efs_file_system" "projeto-efs1" {
#   creation_token = "config-efs" # Usado posteriormente com AWS CLI para montar o EFS
#   tags = {
#     Name = "config"
#   }
# }

# resource "aws_efs_mount_target" "projeto-efs-mt1" {
#   count           = length(module.network.public_subnets)
#   file_system_id  = aws_efs_file_system.projeto-efs1.id
#   subnet_id       = module.network.public_subnets[count.index].id
#   security_groups = [module.security.sg-efs.id]

# }

# # Criando EFS2
# resource "aws_efs_file_system" "projeto-efs2" {
#   creation_token = "cache-efs" # Usado posteriormente com AWS CLI para montar o EFS
#   tags = {
#     Name = "cache"
#   }
# }

# resource "aws_efs_mount_target" "projeto-efs-mt2" {
#   count           = length(module.network.public_subnets)
#   file_system_id  = aws_efs_file_system.projeto-efs2.id
#   subnet_id       = module.network.public_subnets[count.index].id
#   security_groups = [module.security.sg-efs.id]

# }

# Criando EFS3 - Media Full HD
resource "aws_efs_file_system" "projeto-efs3" {
  creation_token = "mediahd-efs" # Usado posteriormente com AWS CLI para montar o EFS
  tags = {
    Name = "mediahd"
  }
}

resource "aws_efs_mount_target" "projeto-efs-mt3" {
  count           = length(module.network.public_subnets)
  file_system_id  = aws_efs_file_system.projeto-efs3.id
  subnet_id       = module.network.public_subnets[count.index].id
  security_groups = [module.security.sg-efs.id]

}

# Criando EFS4 - Media 4K
resource "aws_efs_file_system" "projeto-efs4" {
  creation_token = "media4k-efs" # Usado posteriormente com AWS CLI para montar o EFS
  tags = {
    Name = "media4K"
  }
}

resource "aws_efs_mount_target" "projeto-efs-mt4" {
  count           = length(module.network.public_subnets)
  file_system_id  = aws_efs_file_system.projeto-efs4.id
  subnet_id       = module.network.public_subnets[count.index].id
  security_groups = [module.security.sg-efs.id]

}