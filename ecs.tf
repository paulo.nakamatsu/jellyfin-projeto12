
resource "aws_ecs_task_definition" "jellyfin_task" {
  family                   = "jellyfin"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.ecs-vcpu
  memory                   = var.ecs-memory
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn

  # Depend on the ECR push
  # ECR Removido 
  # depends_on = [null_resource.image_to_ecr]

  # volume {
  #   name = "efs-config"

  #   efs_volume_configuration {
  #     file_system_id = aws_efs_file_system.projeto-efs1.id
  #     # transit_encryption      = "ENABLED"
  #     # transit_encryption_port = 2047
  #   }
  # }

  # volume {
  #   name = "efs-cache"

  #   efs_volume_configuration {
  #     file_system_id = aws_efs_file_system.projeto-efs2.id
  #     # transit_encryption      = "ENABLED"
  #     # transit_encryption_port = 2048
  #   }
  # }

  volume {
    name = "efs-mediahd"

    efs_volume_configuration {
      file_system_id = aws_efs_file_system.projeto-efs3.id
      # transit_encryption      = "ENABLED"
      # transit_encryption_port = 2049
    }
  }

  volume {
    name = "efs-media4k"

    efs_volume_configuration {
      file_system_id = aws_efs_file_system.projeto-efs4.id
      # transit_encryption      = "ENABLED"
      # transit_encryption_port = 2049
    }
  }

  container_definitions = <<DEFINITION
[
  {
    "name": "jellyfin",
    "image": "${var.image-ecr-uri}",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/ecs/jellyfin",
        "awslogs-region": "us-east-1",
        "awslogs-stream-prefix": "ecs"
      }
    },
    
    "mountPoints": [
      {
        "sourceVolume": "efs-mediahd",
        "containerPath": "/mediahd"
      },
      {
        "sourceVolume": "efs-media4k",
        "containerPath": "/media4k"
      }
    ],

    "requires_compatibilities": ["FARGATE"],
    "cpu": ${var.ecs-vcpu},
    "memory": ${var.ecs-memory},
    "essential": true,
    "portMappings": [
      {
        "containerPort": 8096,
        "hostPort": 8096
      }
    ]
  }
]    
DEFINITION
}

resource "aws_ecs_cluster" "jellyfin-cluster" {
  name = "jellyfin-cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_service" "jellyfin_service" {
  name                               = "jellyfin"
  cluster                            = aws_ecs_cluster.jellyfin-cluster.id
  task_definition                    = aws_ecs_task_definition.jellyfin_task.arn
  desired_count                      = 2
  launch_type                        = "FARGATE"
  platform_version                   = "LATEST"
  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 100
  network_configuration {
    subnets = setunion(
      # module.network.private_subnets[*].id,
      module.network.public_subnets[*].id
    )
    security_groups  = [module.security.sg-web.id]
    assign_public_ip = true
  }
  deployment_controller {
    type = "ECS"
  }
  lifecycle {
    // Define a "create_before_destroy" behavior to update the service
    create_before_destroy = true
  }
  load_balancer {
    container_name   = "jellyfin"
    container_port   = 8096
    target_group_arn = aws_lb_target_group.jellyfin.arn
  }
  force_new_deployment = false
}

resource "aws_cloudwatch_log_group" "jellyfin" {
  name              = "/ecs/jellyfin"
  retention_in_days = 3
}

resource "aws_cloudwatch_log_stream" "jellyfin" {
  name           = "ecs"
  log_group_name = aws_cloudwatch_log_group.jellyfin.name
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "ecs-task-execution-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "ecs_task_ecr_policy_attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = aws_iam_policy.ecr_access_policy.arn
}

# resource "aws_iam_role_policy_attachment" "ecs_task_pub_ecr_policy_attachment" {
#   role       = aws_iam_role.ecs_task_execution_role.name
#   policy_arn = aws_iam_policy.ecr_public_access_policy.arn
# }

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs_task_execution_role.name
}

resource "aws_iam_role_policy_attachment" "efs_mount_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonElasticFileSystemClientReadWriteAccess" // Replace with the appropriate EFS policy ARN
  role       = aws_iam_role.ecs_task_execution_role.name
}

resource "aws_iam_role_policy_attachment" "ecs_task_full_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess" // Replace with the appropriate EFS policy ARN
  role       = aws_iam_role.ecs_task_execution_role.name
}

resource "aws_iam_role_policy_attachment" "ecs_task_image_builder" {
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds" // Replace with the appropriate EFS policy ARN
  role       = aws_iam_role.ecs_task_execution_role.name
}

# ELB

resource "aws_lb" "jellyfin" {
  name               = "jellyfin-lb"
  load_balancer_type = "application"
  subnets            = module.network.public_subnets[*].id
  security_groups    = [module.security.sg-elb.id]
}

resource "aws_lb_target_group" "jellyfin" {
  name        = "jellyfin-tg"
  port        = 8096
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = module.network.vpc_id
  health_check {
    path    = "/web/index.html"
    timeout = 20
  }
  stickiness {
    enabled = true
    type    = "lb_cookie"
  }
  depends_on = [aws_lb.jellyfin]
}

resource "aws_lb_listener" "jellyfin" {
  load_balancer_arn = aws_lb.jellyfin.arn
  port              = 8096
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.jellyfin.arn
  }
}

# # ASG
# resource "aws_appautoscaling_target" "ecs_service_as" {
#   max_capacity       = var.max-tasks
#   min_capacity       = var.min-tasks
#   resource_id        = "service/${aws_ecs_cluster.jellyfin-cluster.name}/${aws_ecs_service.jellyfin_service.name}"
#   scalable_dimension = "ecs:service:DesiredCount"
#   service_namespace  = "ecs"
# }

# resource "aws_appautoscaling_policy" "scale-to-cpu" {
#   name               = "scale-to-cpu"
#   policy_type        = "TargetTrackingScaling"
#   resource_id        = aws_appautoscaling_target.ecs_service_as.resource_id
#   scalable_dimension = aws_appautoscaling_target.ecs_service_as.scalable_dimension
#   service_namespace  = aws_appautoscaling_target.ecs_service_as.service_namespace

#   target_tracking_scaling_policy_configuration {
#     scale_in_cooldown  = 30
#     scale_out_cooldown = 30

#     predefined_metric_specification {
#       predefined_metric_type = "ECSServiceAverageCPUUtilization"
#     }

#     target_value = 70
#   }
# }

