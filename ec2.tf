# Cria a instância 1 do Ubuntu 18.04 e instala o Apache com aplicação WEB
resource "aws_instance" "web_server1" {
  count                       = 1
  ami                         = var.ami_ubuntu
  instance_type               = var.instance_type1
  associate_public_ip_address = true
  subnet_id                   = module.network.public_subnets[count.index].id
  vpc_security_group_ids      = [aws_security_group.sg_upload.id]
  key_name                    = "linux_teste"
  depends_on = [aws_efs_mount_target.projeto-efs-mt3,
    aws_efs_mount_target.projeto-efs-mt4,
    aws_efs_file_system.projeto-efs3,
  aws_efs_file_system.projeto-efs4]

  tags = {
    Name = "Upload_server"
  }

  # Script de inicialização que instala o servidor web Apache e aplicação
  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update -y
              sudo apt-get install -y nfs-common
              sudo apt install -y amazon-efs-utils
              sudo mkdir -p /home/ubuntu/mediahd
              sudo mkdir -p /home/ubuntu/media4k
              sudo service rpcbind restart
              echo '${aws_efs_file_system.projeto-efs3.dns_name}:/ /home/ubuntu/mediahd nfs4 defaults,_netdev 0 0' >> /etc/fstab
              echo '${aws_efs_file_system.projeto-efs4.dns_name}:/ /home/ubuntu/media4k nfs4 defaults,_netdev 0 0' >> /etc/fstab
              sleep 60
              sudo mount -a
              sudo chmod -R 777 /home/ubuntu/mediahd
              sudo chmod -R 777 /home/ubuntu/media4k
              EOF
}

# Cria Security Group Web - vpc publica
resource "aws_security_group" "sg_upload" {
  name = "upload-security-group-public"

  vpc_id = module.network.vpc_id

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  tags = {
    Name = "upload-web-security-group-public"
  }
}

