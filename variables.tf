# Arquivo com a definição das variáveis. O arquivo poderia ter qualquer outro nome, ex. valores.tf

variable "regiao" {
  description = "Região da AWS para provisionamento"
  type        = string
  default     = "us-east-1"
}

variable "profile" {
  description = "Profile com as credenciais criadas no IAM"
  type        = string
  default     = "default"
}

variable "tag-base" {
  description = "Nome utilizado para nomenclaruras no projeto"
  type        = string
  default     = "projeto12"
}

variable "health_check" {
  type = map(string)
  default = {
    "timeout"             = "120"
    "interval"            = "20"
    "path"                = "/"
    "port"                = "8096"
    "unhealthy_threshold" = "4"
    "healthy_threshold"   = "3"
  }
}

# S3
variable "nome-bucket" {
  description = "Nome do bucket para configurar no Projeto"
  type        = string
  default     = "projeto-files" # Como o bucket deve ser unico em toda a AWS, sugiro modifica este nome para evitar conflito.
}

variable "min-tasks" {
  description = "Quantidade mínima de tasks no autoscaling"
  type        = number
  default     = 1
}

variable "max-tasks" {
  description = "Quantidade máxima de tasks no autoscaling"
  type        = number
  default     = 2
}

variable "ecs-vcpu" {
  description = "Quantidade máxima de tasks no autoscaling"
  type        = number
  default     = (2 * 1024)
}

variable "ecs-memory" {
  description = "Quantidade máxima de tasks no autoscaling"
  type        = number
  default     = (4 * 1024)
}

variable "image-ecr-uri" {
  description = "Endereço da imagem no ECR"
  type        = string
  default     = "public.ecr.aws/q4b2f1t4/jellyteste1:latest"
}

variable "use-nat-gateway" {
  description = "Especifica se serão criados NAT Gateways para cada Subnet pública."
  type        = bool
  default     = false
}

variable "ami_ubuntu" {
  description = "Canonical, Ubuntu, 22.04 LTS, amd64 jammy image build on 2023-05-16"
  default     = "ami-053b0d53c279acc90"
}

variable "instance_type1" {
  description = "Specifies the AWS instance type."
  default     = "t2.micro"
}