module "security" {
  source     = "./module-web-security"
  region     = var.regiao
  tags-sufix = var.tag-base
  vpc-id     = module.network.vpc_id
}

# Security outputs
# module.security.sg-web
# module.security.sg-elb
# module.security.sg-efs



